<?php
declare(strict_types = 1);

namespace Wsudu\LatteCompiler;

use Latte\Engine;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Wsudu\LatteCompiler\Exceptions\InvalidArgumentException;

class Compiler
{
	const DEFAULT_TEMPORARY_TEMPLATE_FILE_NAME = 'temp.latte';
	const DEFAULT_TEMPLATE_ROOT_DIR_VAR_NAME = 'rootDir';

	/**
	 * @var string|null
	 */
	private $temporaryFilesDir;

	/**
	 * @var string
	 */
	private $temporaryTemplateFileName = self::DEFAULT_TEMPORARY_TEMPLATE_FILE_NAME;

	/**
	 * @var string|null
	 */
	private $templatesRootDirVarName;

	/**
	 * @var string|null
	 */
	private $templatesRootDir;

	public function __construct(string $temporaryFilesDir)
	{
		$this->temporaryFilesDir = $temporaryFilesDir;
	}

	public function compileString(string $string): string
	{
		$latte = new Engine();

		$tempFile = $this->temporaryFilesDir . '/' . $this->temporaryTemplateFileName . mt_rand(0, 10000);
		file_put_contents($tempFile, $string);

		$htmlString = $latte->renderToString($tempFile, [
			$this->templatesRootDirVarName => $this->templatesRootDir,
		]);
		unlink($tempFile);

		return $htmlString;
	}

	/**
	 * @param string[] $stringArray
	 * @return string[]
	 */
	public function compileStringArray(array $stringArray): array
	{
		$htmlArray = [];

		foreach ($stringArray as $key => $string) {
			$htmlArray[$key] = $this->compileString($string);
		}

		return $htmlArray;
	}

	public function compileJson(string $json): string
	{
		try {
			$data = Json::decode($json);
		} catch (JsonException $e) {
			$message = 'JSON not decoded: ' . $e->getMessage();
			throw new InvalidArgumentException($message, $e->getCode(), $e);
		}

		if (is_array($data)) {
			return Json::encode($this->compileStringArray($data));
		} else {
			return Json::encode($this->compileString($data));
		}
	}

	public function setTemplatesRootDir(string $dir, string $varName = self::DEFAULT_TEMPLATE_ROOT_DIR_VAR_NAME)
	{
		$this->templatesRootDir = $dir;
		$this->templatesRootDirVarName = $varName;
	}
}
