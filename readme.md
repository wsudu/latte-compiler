# WSUDU latte compiler

[![Latest Stable Version](https://poser.pugx.org/wsudu/latte-compiler/v/stable)](https://packagist.org/packages/wsudu/latte-compiler)
[![License](https://poser.pugx.org/wsudu/latte-compiler/license)](https://packagist.org/packages/wsudu/latte-compiler)
[![PHPStan](https://img.shields.io/badge/PHPStan-enabled-brightgreen.svg?style=flat)](https://github.com/phpstan/phpstan)

Compile [latte](https://latte.nette.org/) code to HTML. Unlike `\Latte\Loaders\StringLoader`, this compiler can correctly
handle even `{include file.latte}` latte macros in your latte code.

## Installation
Using [composer](https://getcomposer.org):
```
composer require wsudu/latte-compiler
```

## Plain usage
If you have latte file `./templates/myFile.latte`
```latte
<div>Hello {$who}</div>
```

and PHP code below,
```php
<?php

$temporaryFilesDir = __DIR__ . '/temp'; // A writable dir is necessary
$compiler = new \Wsudu\LatteCompiler\Compiler($temporaryFilesDir);

 // Allow using $rootDir variable in templates (e.g. in `{include}` macros)
$compiler->setTemplatesRootDir(__DIR__ . '/templates');

$latteCode = '"{include $rootDir/myFile.latte who => world}"';
$html = $compiler->compileJson($latteCode);
```

the content of `$html` variable is

```html
<div>Hello world</div>
```