<?php
declare(strict_types = 1);

namespace WsuduTests\LatteCompiler;

use Nette\Utils\Json;
use PHPUnit\Framework\TestCase;
use Wsudu\LatteCompiler\Exceptions\InvalidArgumentException;
use Wsudu\LatteCompiler\Compiler;

/**
 * @covers Compiler
 */
class CompilerTest extends TestCase
{

	public function testCompileStringSimpleLatte()
	{
		$inputLatte = '{var $who = world}<div>Hello {$who}</div>';
		$expectedHtml = '<div>Hello world</div>';

		$compiler = $this->createCompiler();

		$outputHtml = $compiler->compileString($inputLatte);

		self::assertSame($expectedHtml, $outputHtml);
	}

	/**
	 * @param string|null $customRootDir
	 * @dataProvider provideDataCompileString
	 */
	public function testCompileString(string $customRootDir = null)
	{
		$rootDirName = $customRootDir ?: Compiler::DEFAULT_TEMPLATE_ROOT_DIR_VAR_NAME;
		$inputLatte = sprintf('{include "$%s/greeting.latte" who => world}', $rootDirName);
		$expectedHtml = '<div>Hello world</div>';

		$compiler = $this->createCompiler();
		if ($customRootDir) {
			$compiler->setTemplatesRootDir(__DIR__ . '/../../assets', $customRootDir);
		} else {
			$compiler->setTemplatesRootDir(__DIR__ . '/../../assets');
		}

		$outputHtml = $compiler->compileString($inputLatte);

		self::assertSame($expectedHtml, $outputHtml);
	}

	public function provideDataCompileString(): array
	{
		return [
			'default root dir' => [],
			'custom root dir' => ['customRootDir'],
		];
	}

	public function testCompileStringArray()
	{
		$inputLatteArray = [
			'{include "$rootDir/greeting.latte" who => world}',
			'{include "$rootDir/greeting.latte" who => man}',
		];
		$expectedHtmlArray = [
			'<div>Hello world</div>',
			'<div>Hello man</div>',
		];

		$compiler = $this->createCompiler();
		$compiler->setTemplatesRootDir(__DIR__ . '/../../assets');

		$outputHtmlArray = $compiler->compileStringArray($inputLatteArray);

		self::assertSame($expectedHtmlArray, $outputHtmlArray);
	}

	/**
	 * @param string $inputJson
	 * @param string $expectedJson
	 * @dataProvider provideDataCompileJson
	 */
	public function testCompileJson(string $inputJson, string $expectedJson)
	{
		$compiler = $this->createCompiler();
		$compiler->setTemplatesRootDir(__DIR__ . '/../../assets');

		$outputJson = $compiler->compileJson($inputJson);

		self::assertSame($expectedJson, $outputJson);
	}

	public function provideDataCompileJson()
	{
		$inputStringJson = Json::encode('{include "$rootDir/greeting.latte" who => world}');
		$expectedStringJson = Json::encode('<div>Hello world</div>');

		$inputArrayJson = Json::encode([
			'{include "$rootDir/greeting.latte" who => world}',
			'{include "$rootDir/greeting.latte" who => man}',
		]);
		$expectedArrayJson = Json::encode([
			'<div>Hello world</div>',
			'<div>Hello man</div>',
		]);

		return [
			'string' => [$inputStringJson, $expectedStringJson],
			'array' => [$inputArrayJson, $expectedArrayJson],
		];
	}

	public function testCompileJsonInvalidArgument()
	{
		self::expectException(InvalidArgumentException::class);

		$compiler = $this->createCompiler();
		$compiler->compileJson('"<div>');
	}

	public function testThereAreNoTempFilesLeft()
	{
		self::assertEmpty(glob($this->getTemporaryFilesDir() . '/*.*'));
	}

	private function createCompiler(): Compiler
	{
		return new Compiler($this->getTemporaryFilesDir());
	}

	private function getTemporaryFilesDir(): string
	{
		return __DIR__ . '/../../temp';
	}
}
