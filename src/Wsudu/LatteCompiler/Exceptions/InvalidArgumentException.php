<?php
declare(strict_types = 1);

namespace Wsudu\LatteCompiler\Exceptions;

class InvalidArgumentException extends \InvalidArgumentException
{
}
